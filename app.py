# -*- coding: utf-8 -*-
import logging
import asyncio
import csv
import sys
import signal

from datetime import datetime
from bleak import BleakClient
from bleak import _logger as logger
from bleak.uuids import uuid16_dict
from bleak import discover


CONROL_POINT_UUID = "2ea78970-7d44-44bb-b097-26183f402409"

ACC_CHAR_UUID = "2ea78970-7d44-44bb-b097-26183f402401"

path = "zog.csv"

async def tryToFindDeviceAndExitOnFail():
	devices = await discover()
	for d in devices:
		if(d.name == "Logger"):
			return d.address
	print("Can't find device")
	raise IOError

fl = False

def disconnectFromDevice(signal, frame):
	global fl
	if (fl):
		exit(0)
	print("That's all, folks")
	fl = True
	
def csv_write(data):
	print(fl);
	print(data);
	
	with open(path, "a", newline='') as csv_file:
		writer = csv.writer(csv_file, delimiter=';')
		for line in data:
			writer.writerow(line)


async def run( loop):
	print( path)
	address = await tryToFindDeviceAndExitOnFail()

	async with BleakClient(address, loop=loop) as client:
		x = await client.is_connected()
		logger.info("Connected: {0}".format(x))
		device = client

		def keypress_handler(sender, data):
			x = data[0] + data[1] * 256;
			if x > 32767:
				x -= 0x10000
			y = data[2] + data[3] * 256;
			if(y > 32767):
				y -= 0x10000
			z = data[4] + data[5] * 256;
			if (z > 32767):
				z -= 0x10000
			
			print("{0}: {1} : {2}".format(x,y,z))
			unix_seconds = (data[6] * 0x1000000 ) + (data[7] * 0x10000 ) + (data[8] *0x100 ) + data[9];
			microseconds = (data[10] *0x100 ) + data[11];
			now = datetime.utcfromtimestamp(unix_seconds);
			data = [["{2}.{1}.{0} {3}:{4}:{5},{6}".format(now.year, now.month, now.day, now.hour, now.minute, now.second, microseconds), x, y, z]]
			csv_write(data);

		write_value = bytearray([0x01])

		await client.write_gatt_char(CONROL_POINT_UUID, write_value, response = True)


		await client.start_notify(ACC_CHAR_UUID, keypress_handler)
		while(not fl):
			await asyncio.sleep(1, loop=loop);
			
		await client.stop_notify(ACC_CHAR_UUID)
		await client.disconnect()
		exit(0);


async def startSync(loop):
	address = await tryToFindDeviceAndExitOnFail()
	
	async with BleakClient(address, loop=loop) as client:
		x = await client.is_connected()
		logger.info("Connected: {0}".format(x))

		write_value = bytearray([0xA1])

		await client.write_gatt_char(CONROL_POINT_UUID, write_value, response = True) 

		await client.disconnect()


async def setTime(loop, timeString):
	address = await tryToFindDeviceAndExitOnFail()

	async with BleakClient(address, loop=loop) as client:
		x = await client.is_connected()
		logger.info("Connected: {0}".format(x))
		time = int(timeString)
		write_value = bytearray([0xAA, (time >> 24) & 0xFF, (time >> 16) & 0xFF, (time >> 8) & 0xFF, time & 0xFF])

		await client.write_gatt_char(CONROL_POINT_UUID, write_value, response = True)
		await client.disconnect()



def showUsage():
	print(" Usage: \n \
	python app.py -s0\n\
	python app.py -s1 time\n\
	python app.py -m file_name.csv\n")


if __name__ == "__main__":
	import os
	signal.signal(signal.SIGINT, disconnectFromDevice)
	signal.signal(signal.SIGABRT, disconnectFromDevice)
	signal.signal(signal.SIGTERM, disconnectFromDevice)
	os.environ["PYTHONASYNCIODEBUG"] = str(1)
	loop = asyncio.get_event_loop()
	fail = False
	try:
		if (len(sys.argv) > 2):
			if (sys.argv[1] == "-m"):
				path = sys.argv[2]
				loop.run_until_complete(run(loop))
			elif(sys.argv[1] == "-s1"):
				loop.run_until_complete(setTime(loop, sys.argv[2]))
			else: 
				showUsage()
		elif len(sys.argv) > 1:
			if(sys.argv[1] == "-s0"):
				loop.run_until_complete(startSync(loop))
			else: 
				showUsage()
		else :
			showUsage()
	except Exception:
		print ("Zog");
		fail = True
	while(fail):
		fail = False
		try:
			if (len(sys.argv) > 2):
				if (sys.argv[1] == "-m"):
					path = sys.argv[2]
					loop.run_until_complete(run(loop))
				elif(sys.argv[1] == "-s1"):
					loop.run_until_complete(setTime(loop, sys.argv[2]))
				else: 
					showUsage()
			elif len(sys.argv) > 1:
				if(sys.argv[1] == "-s0"):
					loop.run_until_complete(startSync(loop))
				else: 
					showUsage()
			else :
				showUsage()
		except Exception as ex:
			print (ex);
			fail = True
	
	